------------------------------------------------------
----------- M O D U L E     B O O K ------------------
------------------------------------------------------



-------------------- book_series ---------------------
create table book_series
(
  id          bigserial     primary key,
  name        varchar(500)  not null,
  description varchar(1023)     null
);

-------------------- book ----------------------------
create table book
(
  id                 bigserial     primary key,
  litres_id          bigint        not null unique,
  name               varchar(500)  not null,
  created_at         date              null,
  book_series        bigint            null references book_series,
  translated_from_id bigint            null references book,
  language_id        bigint        not null references language,
  description        varchar(1023)     null
);

-------------------- book_to_participant -------------
create table book_to_participant
(
  id               bigserial     primary key,
  book_id          bigint        not null references book,
  profile_id       bigint        not null references profile,
  participant_type integer       not null,
  description      varchar(1023)     null,

  unique (book_id, profile_id, participant_type)
);

-------------------- book_genre ----------------------
create table book_genre
(
  id               bigserial     primary key,
  litres_id        bigint        not null unique,
  name             varchar(500)  not null,
  code             varchar(500)      null, -- litres 'token'
  genre_type       integer       not null,
  litres_parent_id bigint            null references book_genre(litres_id)
);

-------------------- book_to_book_genre --------------
create table book_to_genre
(
  book_id       bigint not null references book,
  book_genre_id bigint not null references book_genre,

  unique (book_id, book_genre_id)
);


-- ------------------------------------------------------
-- ----------- M O D U L E     C O P Y ------------------
-- ------------------------------------------------------
--
--
--
-- -------------------- publisher -----------------------
-- create table publisher
-- (
--   id                bigserial     primary key,
--   name              varchar(500)  not null unique,
--   publisher_type_id integer       not null,
--   description       varchar(1023) not null
-- );
--
-- -------------------- copy ----------------------------
-- create table copy
-- (
--   id           bigserial   primary key,
--   book_id      bigint      null references book,
--   publisher_id bigint      null references publisher,
--   created_at   date        null,
--   isbn         varchar(50) null
-- );
--
-- -------------------- copy_audio ----------------------
-- create table copy_audio
-- (
--   id                 bigserial primary key,
--   book_id            bigint    not null references book,
--   copy_id            bigint        null references copy,
--   publisher_id       bigint        null references publisher,
--   created_at         date          null,
--   copy_audio_type_id integer   not null,
--   duration_in_sec    integer       null,
--   format             integer       null,
--   bitrate            smallint      null
-- );
--
-- -------------------- copy_audio_actor ----------------
-- create table copy_audio_actor
-- (
--   copy_audio_id bigint not null references copy_audio,
--   profile_id    bigint not null references profile,
--
--   unique (copy_audio_id, profile_id)
-- );
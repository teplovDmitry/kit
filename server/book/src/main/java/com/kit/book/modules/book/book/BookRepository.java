package com.kit.book.modules.book.book;

import com.kit.core.modules.Repository;

public interface BookRepository extends Repository<Book> {
}

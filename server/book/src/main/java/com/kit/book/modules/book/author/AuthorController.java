package com.kit.book.modules.book.author;

import com.kit.core.modules.application.Application;
import com.kit.core.modules.application.ApplicationLogic;
import com.kit.core.modules.application.ApplicationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

//@RestController
//@RequestMapping(value = "/api/authors")
public class AuthorController {

    private final AuthorValidator authorValidator;
    private final AuthorMapper authorMapper;
    private final ApplicationLogic applicationLogic;

    @Autowired
    public AuthorController(AuthorValidator authorValidator,
                            AuthorMapper authorMapper, ApplicationLogic applicationLogic) {
        this.authorValidator = authorValidator;
        this.authorMapper = authorMapper;
        this.applicationLogic = applicationLogic;
    }

    @PostMapping
    public AuthorDetailsDto create(@RequestBody AuthorDetailsDto dto) {
        Application application = this.applicationLogic.createRequest(ApplicationType.AUTHOR, dto);
        dto.setId(application.getId());
        return dto;
    }
}

package com.kit.book.modules.book.book.genre;

import com.kit.core.modules.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "book_genre")
@Entity
public class Genre extends BaseEntity {

    @Column(name = "litres_id")
    private Long litresId;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "genre_type", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private GenreType genreType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "litres_parent_id", referencedColumnName = "litres_id")
    private Genre litresParent;
}

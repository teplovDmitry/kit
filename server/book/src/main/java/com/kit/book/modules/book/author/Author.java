package com.kit.book.modules.book.author;

import com.kit.core.modules.BaseEntity;
import com.kit.core.modules.profile.Profile;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

//@Entity
//@Table(name = "author")
@Data
public class Author extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "profile_id", nullable = false, unique = true)
    private Profile profile;

    @Column(name = "description", nullable = false)
    private String description;
}

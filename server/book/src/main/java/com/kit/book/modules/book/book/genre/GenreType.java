package com.kit.book.modules.book.book.genre;

public enum GenreType {
    ROOT,
    CONTAINER,
    GENRE;
}

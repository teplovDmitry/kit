package com.kit.book.modules.book.participant;

import java.util.HashMap;
import java.util.Map;

public enum ParticipantType {
    AUTHOR(0),        // автор
    TRANSLATOR(1),    // переводчик
    AGENT(2),         // агент
    PAINTER(3),       // художник
    ORIGINATOR(4),    // составитель
    RETELLER(5),      // пересказчик
    LECTOR(6),        // чтец
    ARTIST(7),        // исполнитель
    MAKER(8),         // производитель
    REDACTOR(9),      // редактор
    ACTOR(10),        // актер
    DIRECTOR(11),     // режиссер
    PRODUCER(12),     // продюсер
    COMPOSER(13),     // композитор
    SOUNDMAN(14),     // звукорежиссер
    SCRIPTSRITER(15); // сценарист

    private int id;

    private static final Map<String, ParticipantType> stringToEnum = new HashMap<>();

    static {
        for (ParticipantType participantType : ParticipantType.values()) {
            stringToEnum.put(participantType.toString(), participantType);
        }
    }

    ParticipantType(int id) {
        this.id = id;
    }
}

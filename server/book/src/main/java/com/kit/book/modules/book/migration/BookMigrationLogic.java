package com.kit.book.modules.book.migration;

import com.kit.book.modules.book.book.Book;
import com.kit.book.modules.book.book.BookService;
import com.kit.book.modules.book.book.genre.GenreService;
import com.kit.book.modules.book.participant.Participant;
import com.kit.book.modules.book.participant.ParticipantType;
import com.kit.core.modules.profile.Profile;
import com.kit.core.modules.profile.ProfileService;
import com.kit.core.system.helpers.FileHelper;
import com.kit.core.system.helpers.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.Optional;

@Service
public class BookMigrationLogic {

    /*ОБРАТИТЬ НА ЭТИ СЛОВА В МИГРАЦИИ ВНИМАНИЕ!!!
    * keywords - тут походу сидят тэги
    * keywords - а это походу серия, в которую входит книга
    * */

    private final BookService bookService;
    private final GenreService genreService;
    private final ProfileService profileService;

    private Book book;
    private Participant author;
    private Profile authorProfile;

    boolean GENRE_IN_PROGRESS = false;
    boolean AUTHOR_IN_PROGRESS = false;
    boolean AUTHOR_FIRST_NAME_IN_PROGRESS = false;
    boolean AUTHOR_LAST_NAME_IN_PROGRESS = false;
    boolean AUTHOR_CODE_IN_PROGRESS = false;

    @Autowired
    public BookMigrationLogic(BookService bookService,
                              GenreService genreService,
                              ProfileService profileService) {
        this.bookService = bookService;
        this.genreService = genreService;
        this.profileService = profileService;
    }

    public void run() throws RuntimeException, XMLStreamException {
        XMLStreamReader reader = FileHelper.getXml("litres_detailed_data.xml");

        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    this.handleStartElement(reader);
                    break;
                case XMLStreamConstants.CHARACTERS:
                    this.handleCurrentValue(reader);
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    this.handleEndElement(reader);
                    break;
                case XMLStreamConstants.START_DOCUMENT:
                    break;
            }
        }
    }

    private void handleStartElement(XMLStreamReader reader) {
        switch (reader.getLocalName()) {
            case "art":
                this.book = this.getBlankBook(reader);
                break;
            case "genre":
                this.GENRE_IN_PROGRESS = true;
                break;
            case "author":
                this.author = new Participant(){{
                    setProfile(new Profile());
                    setType(ParticipantType.AUTHOR);
                }};
                this.AUTHOR_IN_PROGRESS = true;
                break;
            case "first-name":
                this.AUTHOR_FIRST_NAME_IN_PROGRESS = true;
                break;
            case "last-name":
                this.AUTHOR_LAST_NAME_IN_PROGRESS = true;
                break;
            case "id":
                this.AUTHOR_CODE_IN_PROGRESS = true;
                break;
        }
    }

    private void handleEndElement(XMLStreamReader reader) {
        switch (reader.getLocalName()) {
            case "art":
                this.bookService.save(book);
                break;
            case "author":
                this.book.getParticipants().add(this.author);
                this.AUTHOR_IN_PROGRESS = false;
                break;
        }
    }

    private void handleCurrentValue(XMLStreamReader reader) {
        String value = reader.getText().trim();
        if(value.isEmpty())
            return;

        if(this.GENRE_IN_PROGRESS) {
            this.genreService.findByCode(value).ifPresent(x -> this.book.getGenres().add(x));
            this.GENRE_IN_PROGRESS = false;
        }
        if(this.AUTHOR_FIRST_NAME_IN_PROGRESS) {
            this.authorProfile.setFirstName(value);
            this.AUTHOR_FIRST_NAME_IN_PROGRESS = false;
        }
        if(this.AUTHOR_LAST_NAME_IN_PROGRESS) {
            this.authorProfile.setLastName(value);
            this.AUTHOR_LAST_NAME_IN_PROGRESS = false;
        }
        if(this.AUTHOR_CODE_IN_PROGRESS) {
//            this.profileService.findByLitresCode(value).
            Optional<Profile> byLitresCode = this.profileService.findByLitresCode(value);
            if(byLitresCode.isPresent()) {
                this.authorProfile = byLitresCode.get();
            } else {
                this.authorProfile.setLitresCode(value);
            }
            this.AUTHOR_CODE_IN_PROGRESS = false;
        }
    }

    private Book getBlankBook(XMLStreamReader reader) {
        Book result = new Book();

        for (int i = 0; i < reader.getAttributeCount() - 1; i++) {
            if(reader.getAttributeLocalName(i) == null) {
                continue;
            }
            String attributeName = reader.getAttributeLocalName(0);
            String attributeValue = reader.getAttributeValue(0);
            switch (attributeName) {
                case "int_id":
                    result.setLitresId(StringHelper.getLong(attributeValue));
                    break;
            }
        }

        return result;
    }
}
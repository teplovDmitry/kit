package com.kit.book.system.configs;

import com.kit.core.system.configs.FlywayConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BookFlywayConfig implements FlywayConfig {

    public String getSchema() {
        return "public";
    }

    public String getTable() {
        return "schema_version_book";
    }

    public String getLocation() {
        return "db/migration/book";
    }

    public boolean getBaselineOnMigrate() {
        return true;
    }
}

package com.kit.book.modules.book.migration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootVersion;
import org.springframework.core.SpringVersion;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

@RestController
@RequestMapping(value = "/api/book/migration")
public class BookMigrationController {

    private final BookGenreMigrationLogic bookGenreMigrationLogic;
    private final BookGenreMigrationLogic2 bookGenreMigrationLogic2;
    private final BookMigrationLogic bookMigrationLogic;

    @Autowired
    public BookMigrationController(BookGenreMigrationLogic bookGenreMigrationLogic,
                                   BookGenreMigrationLogic2 bookGenreMigrationLogic2,
                                   BookMigrationLogic bookMigrationLogic) {
        this.bookGenreMigrationLogic = bookGenreMigrationLogic;
        this.bookGenreMigrationLogic2 = bookGenreMigrationLogic2;
        this.bookMigrationLogic = bookMigrationLogic;
    }

    @PostMapping
    public String create() throws IOException, SAXException, ParserConfigurationException, XMLStreamException {
//        this.bookGenreMigrationLogic.run();
//        this.bookGenreMigrationLogic2.run();
//        this.bookMigrationLogic.run();
        return SpringBootVersion.getVersion() + "______" + SpringVersion.getVersion();
//        return SpringVersion.getVersion();
//        return "Migration genre done";
    }
}

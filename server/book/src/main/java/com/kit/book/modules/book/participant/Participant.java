package com.kit.book.modules.book.participant;

import com.kit.book.modules.book.book.Book;
import com.kit.core.modules.BaseEntity;
import com.kit.core.modules.profile.Profile;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "book_to_participant",
       uniqueConstraints = @UniqueConstraint(columnNames = {"book_id", "profile_id", "participant_type"}))
@Data
public class Participant extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id", nullable = false)
    private Book book;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "profile_id", nullable = false)
    private Profile profile;

    @Column(name = "participant_type", nullable = false)
    @Enumerated
    private ParticipantType type;

    @Column(name = "description", nullable = false)
    private String description;
}

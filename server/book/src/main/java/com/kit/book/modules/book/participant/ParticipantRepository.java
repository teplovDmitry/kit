package com.kit.book.modules.book.participant;

import com.kit.core.modules.Repository;

public interface ParticipantRepository extends Repository<Participant> {
}

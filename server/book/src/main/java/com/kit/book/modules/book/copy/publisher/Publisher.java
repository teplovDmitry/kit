package com.kit.book.modules.book.copy.publisher;//package com.mylib.modules.book.copy.publisher;
//
//import com.mylib.modules.BaseEntity;
//import lombok.Data;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.EnumType;
//import javax.persistence.Enumerated;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "publisher")
//@Data
//public class Publisher extends BaseEntity {
//
//    @JoinColumn(name = "name", nullable = false)
//    private String name;
//
//    @Column(name = "publisher_type_id", nullable = false)
//    @Enumerated
//    private PublisherType publisherType;
//
//    @Column(name = "description", nullable = false)
//    private String description;
//}

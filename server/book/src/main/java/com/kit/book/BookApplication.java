package com.kit.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.kit")
@EntityScan("com.kit")
public class BookApplication {

    public static void main(String[] args) {
		SpringApplication.run(BookApplication.class, args);
	}
}

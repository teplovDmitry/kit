package com.kit.book.modules.book.migration;

import com.kit.book.modules.book.book.genre.Genre;
import com.kit.book.modules.book.book.genre.GenreService;
import com.kit.book.modules.book.book.genre.GenreType;
import com.kit.core.system.exceptions.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Objects;
import java.util.Optional;

@Service
public class BookGenreMigrationLogic2 {

    private static final String BOOK_GENRES_XML_URL = "https://partnersdnld.litres.ru/genres_list_2/";

    private final GenreService genreService;

    @Autowired
    public BookGenreMigrationLogic2(GenreService genreService) {
        this.genreService = genreService;
    }

    public void run() throws ParserConfigurationException, SAXException, IOException {
        URL url = new URL(BOOK_GENRES_XML_URL);
        URLConnection urlConnection = url.openConnection();
        InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputStream);
        doc.getDocumentElement().normalize();

        NodeList children = doc.getFirstChild().getChildNodes();
        this.handleChildren(children, null);
    }

    private void handleChildren(NodeList children, Genre genreParent) {
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            Element genreElement = (Element) child;

            Genre newGenre = this.getNewGenre(genreElement, genreParent);
            Optional<Genre> alreadyExistGenre = this.genreService.findByLitresId(newGenre.getLitresId());
            if (alreadyExistGenre.isPresent()) {
                this.updateIfNeeded(alreadyExistGenre.get(), newGenre);
                return;
            }

            Genre savedGenre = this.genreService.save(newGenre);

            if(child.getChildNodes().getLength() > 0) {
                this.handleChildren(child.getChildNodes(), savedGenre);
            }
        }
    }

    private Genre getNewGenre(Element genreElement, Genre genreParent) {
        Long litresId = Long.parseLong(genreElement.getAttribute("id"));
        String name = genreElement.getAttribute("title");
        String code = Objects.toString(genreElement.getAttribute("token"), "");
        GenreType genreType = this.getGenreTypeByAttribute(genreElement);
        return new Genre(litresId, name, code, genreType, genreParent);
    }

    private GenreType getGenreTypeByAttribute(Element genreElement) {
        GenreType result;
        String type = genreElement.getAttribute("type");
        switch (type) {
            case "root":
                result = GenreType.ROOT;
                break;
            case "container":
                result = GenreType.CONTAINER;
                break;
            case "genre":
                result = GenreType.GENRE;
                break;
            default:
                throw new ApiException(HttpStatus.NOT_IMPLEMENTED, "501",
                        String.format("Type of genre %s not found", type));
        }
        return result;
    }

    private void updateIfNeeded(Genre genreInDb, Genre newGenre) {
        if (this.isGenresEquals(genreInDb, newGenre)) {
            return;
        }

        if (!genreInDb.getName().equals(newGenre.getName())) {
            genreInDb.setName(newGenre.getName());
        }
        if (!genreInDb.getCode().equals(newGenre.getCode())) {
            genreInDb.setCode(newGenre.getCode());
        }
        if (genreInDb.getGenreType() != newGenre.getGenreType()) {
            genreInDb.setGenreType(newGenre.getGenreType());
        }

        this.genreService.save(genreInDb);
    }

    private boolean isGenresEquals(Genre genreInDb, Genre newGenre) {
        return genreInDb.getName().equals(newGenre.getName())
                && genreInDb.getCode().equals(newGenre.getCode())
                && genreInDb.getGenreType() == newGenre.getGenreType();
    }
}
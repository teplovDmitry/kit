package com.kit.book.modules.book.book.genre;

import com.kit.core.modules.Repository;

import java.util.Optional;

public interface GenreRepository extends Repository<Genre> {

    Optional<Genre> findByLitresId(Long id);

    Optional<Genre> findByCode(String code);
}

package com.kit.book.modules.book.book;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kit.book.modules.book.book.genre.Genre;
import com.kit.book.modules.book.participant.Participant;
import com.kit.book.modules.book.series.Series;
import com.kit.core.modules.BaseEntity;
import com.kit.core.modules.common.entity.Language;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "book")
@Data
public class Book extends BaseEntity {

    @Column(name = "litres_id", nullable = false, unique = true)
    private Long litresId;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_series")
    private Series series;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "translated_from_id")
    private Book originBook;

    @OneToMany(mappedBy="originBook", fetch = FetchType.LAZY)
    private List<Book> translatedBooks = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "language_id")
    private Language language;

    @Column(name = "description")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "book_to_genre",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "book_genre_id"))
    @JsonIgnore
    private List<Genre> genres;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "book_to_participant",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "profile_id"))
    @JsonIgnore
    private List<Participant> participants;
}

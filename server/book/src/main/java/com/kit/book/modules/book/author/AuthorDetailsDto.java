package com.kit.book.modules.book.author;

import com.kit.core.modules.profile.ProfileDetailsDto;
import com.kit.core.system.dto.BaseDto;

//@Data
public class AuthorDetailsDto extends BaseDto {
    private ProfileDetailsDto profile;
}

package com.kit.book.modules.book.translator;

import com.kit.core.modules.BaseEntity;
import com.kit.core.modules.profile.Profile;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

//import javax.persistence.Entity;
//import javax.persistence.Table;

//@Entity
//@Table(name = "translator")
//@Data
public class Translator extends BaseEntity {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "profile_id", nullable = false, unique = true)
    private Profile profile;

    @Column(name = "description", nullable = false)
    private String description;
}

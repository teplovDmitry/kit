package com.kit.book.modules.book.book.genre;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GenreService {

    private final GenreRepository genreRepository;

    @Autowired
    public GenreService(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    public Genre save(Genre genre) {
        return this.genreRepository.save(genre);
    }

    public Optional<Genre> findByLitresId(Long id) {
        return this.genreRepository.findByLitresId(id);
    }

    public Optional<Genre> findByCode(String code) {
        return this.genreRepository.findByCode(code);
    }
}

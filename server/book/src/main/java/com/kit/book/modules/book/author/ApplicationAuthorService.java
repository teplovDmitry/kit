package com.kit.book.modules.book.author;

import com.kit.core.modules.application.ApplicationHandler;
import com.kit.core.modules.application.ApplicationType;
import com.kit.core.system.dto.BaseDto;
import com.kit.core.system.json.ExtraJson;

//@Component
public class ApplicationAuthorService implements ApplicationHandler {

    @Override
    public ExtraJson getContent(BaseDto dto) {
//        AuthorDetailsDto authorDto = (AuthorDetailsDto) dto;
//        ProfileDetailsDto profile = authorDto.getProfile();
//
//        ExtraJson result = new ExtraJson();
//
//        String json = String.format("{\"profileType\": \"%1$s\"," +
//                "\"firstName\": \"%2$s\"," +
//                "\"middleName\": \"%3$s\"," +
//                "\"lastName\": \"%4$s\"," +
//                "\"birthDate\": \"%5$s\"}",
//                profile.getProfileType(),
//                profile.getFirstName(),
//                profile.getMiddleName(),
//                profile.getLastName(),
//                profile.getBirthDate());
//
//        result.put("profile", json);
//        return result;
        return null;
    }

    @Override
    public ApplicationType getApplicationType() {
        return ApplicationType.AUTHOR;
    }
}

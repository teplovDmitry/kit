------------------------------------------------------
----------- M O D U L E     P R O F I L E ------------
------------------------------------------------------



-------------------- profile -------------------------
create table profile
(
  id                 bigserial    primary key,
  first_name         varchar(255)     null,
  middle_name        varchar(255)     null,
  last_name          varchar(255)     null,
  birth_date         date             null,
  translated_from_id bigint           null references profile,
  litres_code        varchar(255)     null unique -- litres id, for example 'e00dfc87-2a80-102a-9ae1-2dfe723fe7c7'
);

insert into profile (first_name, last_name, birth_date)
values ('Anakin', 'Skywalker', now());

-------------------- permission ----------------------
create table permission
(
  id          bigserial    primary key,
  name        varchar(31)  not null unique,
  module_type integer      not null,
  description varchar(355) not null
);

-------------------- role ----------------------------
create table role
(
  id   bigserial    primary key,
  name varchar(255) not null unique
);

insert into role(name)
values ('admin');

-------------------- role_permission ------------------
create table permission_to_role
(
  id            bigserial primary key,
  role_id       bigint    not null references role,
  permission_id bigint    not null references permission,

  unique (role_id, permission_id)
);

-------------------- users ----------------------------
create table users (
  id            bigserial               primary key,
  username      varchar(50)             not null unique,
  email         varchar(50)             not null unique,
  password_hash varchar(60)             not null,
  profile_id    bigint                  not null references profile,
  role_id       bigint                  not null references role,
  created_at    timestamp default now() not null,
  deleted_at    timestamp                   null
);

insert into users (username, email, password_hash, profile_id, role_id, created_at)
values ('admin', 'admin@admin.com', '$2a$10$XmtWixcSIQVNuX.j3SY7ZegiojYcKp.yE1MtqgF7VAy6e1GclZITm', 1, 1, now());



------------------------------------------------------
----------- M O D U L E     C O M M O N --------------
------------------------------------------------------



-------------------- language & country --------------
create table language
(
  id   bigserial    primary key,
  code varchar(2)   not null unique,
  name varchar(255) not null unique
);

insert into language (code, name)
values
  ('ru', 'Русский'    ), ('uk', 'Украинский'     ), ('en', 'Английский'     ), ('de', 'Немецкий'         ), ('fr', 'Французский'  ),
  ('ab', 'Абхазский'  ), ('az', 'Азербайджанский'), ('ay', 'Аймара'         ), ('sq', 'Албанский'        ), ('ar', 'Арабский'     ),
  ('hy', 'Армянский'  ), ('as', 'Ассамский'      ), ('af', 'Африкаанс'      ), ('ts', 'Банту'            ), ('eu', 'Баскский'     ),
  ('ba', 'Башкирский' ), ('be', 'Белорусский'    ), ('bn', 'Бенгальский'    ), ('my', 'Бирманский'       ), ('bh', 'Бихарский'    ),
  ('bg', 'Болгарский' ), ('br', 'Бретонский'     ), ('cy', 'Валлийский'     ), ('hu', 'Венгерский'       ), ('wo', 'Волоф'        ),
  ('vi', 'Вьетнамский'), ('gd', 'Гаэльский'      ), ('nl', 'Голландский'    ), ('el', 'Греческий'        ), ('ka', 'Грузинский'   ),
  ('gn', 'Гуарани'    ), ('da', 'Датский'        ), ('gr', 'Древнегреческий'), ('iw', 'Древнееврейский'  ), ('dr', 'Древнерусский'),
  ('zu', 'Зулу'       ), ('he', 'Иврит'          ), ('yi', 'Идиш'           ), ('in', 'Индонезийский'    ), ('ia', 'Интерлингва'  ),
  ('ga', 'Ирландский' ), ('is', 'Исландский'     ), ('es', 'Испанский'      ), ('it', 'Итальянский'      ), ('kk', 'Казахский'    ),
  ('kn', 'Каннада'    ), ('ca', 'Каталанский'    ), ('ks', 'Кашмири'        ), ('qu', 'Кечуа'            ), ('ky', 'Киргизский'   ),
  ('zh', 'Китайский'  ), ('ko', 'Корейский'      ), ('kw', 'Корнский'       ), ('co', 'Корсиканский'     ), ('ku', 'Курдский'     ),
  ('km', 'Кхмерский'  ), ('xh', 'Кхоса'          ), ('la', 'Латинский'      ), ('lv', 'Латышский'        ), ('lt', 'Литовский'    ),
  ('mk', 'Македонский'), ('mg', 'Малагасийский'  ), ('ms', 'Малайский'      ), ('mt', 'Мальтийский'      ), ('mi', 'Маори'        ),
  ('mr', 'Маратхи'    ), ('mo', 'Молдавский'     ), ('mn', 'Монгольский'    ), ('na', 'Науру'            ), ('ne', 'Непали'       ),
  ('no', 'Норвежский' ), ('pa', 'Панджаби'       ), ('fa', 'Персидский'     ), ('pl', 'Польский'         ), ('pt', 'Португальский'),
  ('ps', 'Пушту'      ), ('rm', 'Ретороманский'  ), ('ro', 'Румынский'      ), ('rn', 'Рунди'            ), ('sm', 'Самоанский'   ),
  ('sa', 'Санскрит'   ), ('sr', 'Сербский'       ), ('si', 'Сингальский'    ), ('sd', 'Синдхи'           ), ('sk', 'Словацкий'    ),
  ('sl', 'Словенский' ), ('so', 'Сомали'         ), ('st', 'Сото'           ), ('sw', 'Суахили'          ), ('su', 'Сунданский'   ),
  ('tl', 'Тагальский' ), ('tg', 'Таджикский'     ), ('th', 'Тайский'        ), ('ta', 'Тамильский'       ), ('tt', 'Татарский'    ),
  ('te', 'Телугу'     ), ('bo', 'Тибетский'      ), ('tr', 'Турецкий'       ), ('tk', 'Туркменский'      ), ('uz', 'Узбекский'    ),
  ('ug', 'Уйгурский'  ), ('ur', 'Урду'           ), ('fo', 'Фарерский'      ), ('fj', 'Фиджи'            ), ('fi', 'Финский'      ),
  ('fy', 'Фризский'   ), ('ha', 'Хауса'          ), ('hi', 'Хинди'          ), ('hr', 'Хорватскосербский'), ('cs', 'Чешский'      ),
  ('sv', 'Шведский'   ), ('sn', 'Шона'           ), ('eo', 'Эсперанто'      ), ('et', 'Эстонский'        ), ('jv', 'Яванский'     ),
  ('ja', 'Японский');

create table country
(
  id   bigserial    primary key,
  name varchar(255) not null unique
);

-------------------- applications --------------------
create table application
(
  id              bigserial   primary key,
  type            integer   not null,
  user_id         bigint    not null references users,
  created_at      timestamp not null default now(),
  content         jsonb     not null,
  entity_id       bigint        null -- id of created entity, after app was approved
);

create table application_history
(
  id              bigserial primary key,
  application_id  bigint    not null references application,
  check_user_id   bigint    not null references users,
  check_at        timestamp not null default now(),
  content         jsonb     not null,
  status          integer   not null
);
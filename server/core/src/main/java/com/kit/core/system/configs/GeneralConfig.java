package com.kit.core.system.configs;

import com.kit.core.modules.user_role_permission.user.User;
import com.kit.core.security.UserAuditorAwareImpl;
import com.kit.core.system.helpers.DateHelper;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.text.SimpleDateFormat;

@Configuration
@EnableJpaAuditing
@EnableJpaRepositories("com.kit")
@SuppressWarnings("unused")
public class GeneralConfig {

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder result = new Jackson2ObjectMapperBuilder();
        result.indentOutput(true).dateFormat(new SimpleDateFormat(DateHelper.DATE_FORMAT));
        return result;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuditorAware<User> auditorProvider() {
        return new UserAuditorAwareImpl();
    }

    @Bean
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }
}

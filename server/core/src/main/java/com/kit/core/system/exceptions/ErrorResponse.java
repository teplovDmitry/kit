package com.kit.core.system.exceptions;

class ErrorResponse {
    private int httpStatus;
    private String errorCode;
    private String message;

    ErrorResponse(int httpStatus,
                  String errorCode,
                  String message){
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
        this.message = message;
    }
}
package com.kit.core.system.json;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

@Data
public class ExtraJson extends HashMap<String, Object> implements Serializable {
}

package com.kit.core.modules.application;

public enum ApplicationStatus {
    /**
     * Application was handle and return for rework
     */
    PENDING,
    /**
     * Application is reviewed by a moderator
     */
    IN_PROGRESS,
    /**
     * Application was approved
     */
    APPROVED
}

package com.kit.core.system.helpers;

import com.kit.core.modules.application.ApplicationHandler;
import com.kit.core.modules.application.ApplicationType;
import com.kit.core.system.exceptions.ApiException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ContextHelper implements ApplicationContextAware {

    private static ApplicationContext context;

    public static <T> List<T> getBeans(Class<T> beanClass){
        return new ArrayList<>(context.getBeansOfType(beanClass).values());
    }

    public static ApplicationHandler getApplicationHandler(ApplicationType applicationType){
        return getBeans(ApplicationHandler.class)
                .stream()
                .filter(x -> x.getApplicationType() == applicationType)
                .findFirst()
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, "404",
                        String.format("Type of application %s not found", applicationType)));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}

package com.kit.core.system.responses;

public class ApiResponse <T> {
    private T data;

    public ApiResponse(T data){
        this.data = data;
    }
    public T getData() {
        return data;
    }
}
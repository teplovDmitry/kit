package com.kit.core.modules.common.enums;

public enum EntityStatus {
    ACTIVE,
    PENDING,
    TRANSLATOR
}

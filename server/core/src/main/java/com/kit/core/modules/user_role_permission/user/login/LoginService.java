package com.kit.core.modules.user_role_permission.user.login;

import com.kit.core.modules.user_role_permission.user.User;
import com.kit.core.modules.user_role_permission.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService {

    private final UserService userService;

    @Autowired
    public LoginService(UserService userService) {
        this.userService = userService;
    }

    public Optional<User> login(LoginDto loginDto) {
        return this.userService.findByEmail(loginDto.getEmail())
                .filter(user -> BCrypt.checkpw(loginDto.getPassword(), user.getPasswordHash()));
    }
}

package com.kit.core.modules.user_role_permission.permission;

import com.kit.core.modules.Repository;

public interface PermissionRoleRepository extends Repository<PermissionRole> {
    void deleteByPermissionId(Long permissionId);
}

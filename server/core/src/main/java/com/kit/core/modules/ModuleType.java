package com.kit.core.modules;

public enum ModuleType {
    CORE,
    USER_ROLE_PERMISSION
}

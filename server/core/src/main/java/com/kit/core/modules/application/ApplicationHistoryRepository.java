package com.kit.core.modules.application;

import com.kit.core.modules.Repository;

public interface ApplicationHistoryRepository extends Repository<ApplicationHistory> {
}

package com.kit.core.modules.user_role_permission.permission;

import com.kit.core.modules.BaseEntity;
import com.kit.core.modules.ModuleType;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "permission")
public class Permission extends BaseEntity {

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "module_type", nullable = false)
    @Enumerated
    private ModuleType moduleType;

    @Column(name = "description", nullable = false)
    private String description;
}

package com.kit.core.system.configs;

public interface FlywayConfig {

    String getSchema();

    String getTable();

    String getLocation();

    boolean getBaselineOnMigrate();
}

package com.kit.core.system.helpers;

public class StringHelper {

    public static int getInt(String value) {
        return Integer.parseInt(value.trim());
    }

    public static Long getLong(String value) {
        return Long.parseLong(value.trim());
    }
}

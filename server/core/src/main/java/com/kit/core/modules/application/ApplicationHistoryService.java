package com.kit.core.modules.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationHistoryService {

    private final ApplicationHistoryRepository applicationHistoryRepository;

    @Autowired
    public ApplicationHistoryService(ApplicationHistoryRepository applicationHistoryRepository) {
        this.applicationHistoryRepository = applicationHistoryRepository;
    }

    public ApplicationHistory save(ApplicationHistory applicationHistory) {
        return this.applicationHistoryRepository.save(applicationHistory);
    }
}

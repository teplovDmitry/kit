package com.kit.core.system.helpers;

import com.kit.core.system.exceptions.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

@Component
public class FileHelper {

    public static XMLStreamReader getXml(String scriptName) throws ApiException {
        return getScript(scriptName );
    }

    private static XMLStreamReader getScript(String fileName) throws ApiException {
        try {
            FileHelper thisClass = new FileHelper();
            InputStream is = thisClass.getClass().getClassLoader().getResourceAsStream("db/" + "xml" + "/" + fileName);
            Reader reader = new BufferedReader(new InputStreamReader(is));
            XMLInputFactory factory = XMLInputFactory.newInstance();
            return factory.createXMLStreamReader(reader);
        } catch (Exception e) {
            throw new ApiException(HttpStatus.NOT_IMPLEMENTED, "501",
                    String.format("File %s not found", fileName));
        }
    }
}
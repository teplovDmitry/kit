package com.kit.core.modules;

import com.kit.core.system.responses.ApiResponse;

public abstract class BaseController {
    protected <T> ApiResponse<T> ReturnResponse(T data){
        return new ApiResponse<T>(data);
    }
}

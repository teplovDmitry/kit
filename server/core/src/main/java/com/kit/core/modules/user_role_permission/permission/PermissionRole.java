package com.kit.core.modules.user_role_permission.permission;

import com.kit.core.modules.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Data
@Table(name = "permission_to_role",
       uniqueConstraints = @UniqueConstraint(columnNames = {"role_id", "permission_id"}))
class PermissionRole extends BaseEntity {

    @Column(name = "role_id", nullable = false)
    private Long roleId;

    @Column(name = "permission_id", nullable = false)
    private Long permissionId;
}

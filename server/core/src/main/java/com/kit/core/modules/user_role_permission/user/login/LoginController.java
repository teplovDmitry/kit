package com.kit.core.modules.user_role_permission.user.login;

import com.kit.core.modules.BaseController;
import com.kit.core.security.TokenHelper;
import com.kit.core.system.exceptions.InvalidCredentialsException;
import com.kit.core.system.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SuppressWarnings("unused")
public class LoginController extends BaseController {

    private final LoginService loginService;
    private final TokenHelper tokenHelper;

    @Autowired
    public LoginController(LoginService loginService, TokenHelper tokenHelper) {
        this.loginService = loginService;
        this.tokenHelper = tokenHelper;
    }

    @PostMapping(path = "/api/login")
    public ApiResponse<String> login(@RequestBody LoginDto loginDto) throws InvalidCredentialsException {
        return this.loginService.login(loginDto)
                .map(user -> ReturnResponse(this.tokenHelper.tokenFor(user)))
                .orElseThrow(InvalidCredentialsException::new);
    }
}

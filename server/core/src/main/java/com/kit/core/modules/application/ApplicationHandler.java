package com.kit.core.modules.application;

import com.kit.core.system.dto.BaseDto;
import com.kit.core.system.json.ExtraJson;

public interface ApplicationHandler {

    ExtraJson getContent(BaseDto dto);

    ApplicationType getApplicationType();
}

package com.kit.core.modules.common.entity;

import com.kit.core.modules.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "country")
@Data
public class Country extends BaseEntity {

    @Column(name = "name", nullable = false, unique = true)
    private String name;
}

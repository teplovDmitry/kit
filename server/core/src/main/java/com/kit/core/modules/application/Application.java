package com.kit.core.modules.application;

import com.kit.core.modules.BaseEntity;
import com.kit.core.modules.user_role_permission.user.User;
import com.kit.core.system.helpers.DateHelper;
import com.kit.core.system.helpers.UserHelper;
import com.kit.core.system.json.ExtraJson;
import com.kit.core.system.json.ExtraJsonType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "application")
@Data
@TypeDef(name = "ExtraJsonType", typeClass = ExtraJsonType.class)
public class Application extends BaseEntity {

    @Column(name = "type", nullable = false)
    @Enumerated
    private ApplicationType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @Type(type = "ExtraJsonType")
    @Column(name = "content", columnDefinition = "jsonb", nullable = false)
    private ExtraJson content;

    @Column(name = "entity_id")
    private Long entityId;

    public Application(ApplicationType type, ExtraJson content) {
        this.type = type;
        this.content = content;
        this.createdAt = DateHelper.getLocalDateTimeNow();
        this.user = UserHelper.getCurrentUser();
    }
}

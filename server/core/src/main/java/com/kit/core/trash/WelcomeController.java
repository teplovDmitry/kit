package com.kit.core.trash;

import com.kit.core.modules.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController extends BaseController {
    @RequestMapping(value = "/api", method = RequestMethod.GET)
    public String get(){
        return "Welcome to kit!";
    }
}

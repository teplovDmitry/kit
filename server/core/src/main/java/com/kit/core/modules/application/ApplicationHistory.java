package com.kit.core.modules.application;

import com.kit.core.modules.BaseEntity;
import com.kit.core.modules.user_role_permission.user.User;
import com.kit.core.system.helpers.DateHelper;
import com.kit.core.system.json.ExtraJson;
import com.kit.core.system.json.ExtraJsonType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "application_history")
@Data
@TypeDef(name = "ExtraJsonType", typeClass = ExtraJsonType.class)
public class ApplicationHistory extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id", nullable = false)
    private Application application;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "check_user_id", nullable = false)
    private User checkUser;

    @Column(name = "check_at", nullable = false)
    private LocalDateTime checkAt = DateHelper.getLocalDateTimeNow();

    @Type(type = "ExtraJsonType")
    @Column(name = "content", columnDefinition = "jsonb")
    private ExtraJson content;

    @Column(name = "status", nullable = false)
    @Enumerated
    private ApplicationStatus status;
}

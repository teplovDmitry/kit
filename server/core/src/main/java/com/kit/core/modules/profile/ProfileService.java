package com.kit.core.modules.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProfileService {

    private final ProfileRepository profileRepository;

    @Autowired
    public ProfileService(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    public Profile save(Profile profile ) {
        return this.profileRepository.save(profile);
    }

    public Optional<Profile> findByLitresCode(String litresCode) {
        return this.profileRepository.findByLitresCode(litresCode);
    }
}

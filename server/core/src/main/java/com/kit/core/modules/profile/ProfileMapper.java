package com.kit.core.modules.profile;

import com.kit.core.modules.Mapper;
import org.modelmapper.ModelMapper;

@Mapper
public class ProfileMapper {

    public Profile mapToEntity(ProfileDetailsDto dto) {
        return new ModelMapper().map(dto, Profile.class);
    }

    public ProfileDetailsDto mapToDto(Profile profile) {
        return new ModelMapper().map(profile, ProfileDetailsDto.class);
    }
}

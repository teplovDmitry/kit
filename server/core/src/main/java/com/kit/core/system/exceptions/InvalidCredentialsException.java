package com.kit.core.system.exceptions;

public class InvalidCredentialsException extends UnauthorizedException {
    public InvalidCredentialsException() {
        super("invalid_credentials", "Invalid username or passwordHash.");
    }
}

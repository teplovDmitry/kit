package com.kit.core.modules.profile;

import com.kit.core.system.dto.BaseDto;
import lombok.Data;

import java.time.LocalDate;

@Data
public class ProfileDetailsDto extends BaseDto{
    private String firstName;
    private String middleName;
    private String lastName;
    private LocalDate birthDate;
}

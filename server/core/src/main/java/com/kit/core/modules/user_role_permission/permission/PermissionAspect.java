package com.kit.core.modules.user_role_permission.permission;

import com.kit.core.modules.user_role_permission.user.User;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;

@Aspect
@Component
public class PermissionAspect {

    @Before("@annotation(com.kit.core.modules.user_role_permission.permission.PermissionRequired)")
    public void checkAllPermissions(JoinPoint joinPoint) throws Exception {
        User currentUser = (User)Arrays.stream(joinPoint.getArgs())
                .filter(x -> x instanceof User)
                .findFirst()
                .orElseThrow(() -> new Exception("User parameter was not set"));

        if(currentUser.isAdmin())
            return;

        Method method = MethodSignature.class.cast(joinPoint.getSignature()).getMethod();
        if (!currentUser.hasPermission(method.getAnnotation(PermissionRequired.class).name())) {
            throw new Exception("You do not have permissions - " + method.getAnnotation(PermissionRequired.class).name());
        }
    }
}
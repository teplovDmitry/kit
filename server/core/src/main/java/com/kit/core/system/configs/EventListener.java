package com.kit.core.system.configs;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.event.spi.PersistEvent;
import org.hibernate.event.spi.PersistEventListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class EventListener implements PersistEventListener {

    @Override
    public void onPersist(PersistEvent event) throws HibernateException {

    }

    @Override
    public void onPersist(PersistEvent event, Map createdAlready) throws HibernateException {

    }
}
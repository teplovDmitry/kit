package com.kit.core.system.helpers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateHelper {

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd (HH:mm)";
    private static DateTimeFormatter dateFormatter;
    private static DateTimeFormatter dateTimeFormatter;

    static {
        dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
    }

    public static String convert(LocalDate value) {
        return value.format(dateFormatter);
    }

    public static String convert(LocalDateTime value) {
        return value.format(dateTimeFormatter);
    }

    private static Date localDateToDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime getLocalDateTimeNow() {
        return LocalDateTime.now();
    }

    public static LocalDate getLocalDateNow() {
        return LocalDate.now();
    }

    public static LocalTime getLocalTimeNow() {
        return LocalTime.now();
    }
}

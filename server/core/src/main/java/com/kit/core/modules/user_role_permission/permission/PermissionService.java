package com.kit.core.modules.user_role_permission.permission;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PermissionService {

    private final PermissionRepository permissionRepository;
    private final PermissionRoleRepository permissionRoleRepository;

    public PermissionService(PermissionRepository permissionRepository,
                             PermissionRoleRepository permissionRoleRepository) {
        this.permissionRepository = permissionRepository;
        this.permissionRoleRepository = permissionRoleRepository;
    }

    public List<Permission> findAll() {
        return this.permissionRepository.findAll();
    }

    public void save(Permission permission) {
        this.permissionRepository.save(permission);
    }

    @Transactional
    public void delete(Permission permission) {
        this.permissionRoleRepository.deleteByPermissionId(permission.getId());
        this.permissionRepository.delete(permission);
    }
}

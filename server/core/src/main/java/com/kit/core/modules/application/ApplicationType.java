package com.kit.core.modules.application;

public enum ApplicationType {
    AUTHOR,
    BOOK
}

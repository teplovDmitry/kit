package com.kit.core.system.exceptions;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends ApiException {
    public UnauthorizedException() {
        this("Unauthorized access.");
    }

    public UnauthorizedException(String message) {
        this("unauthorized_access", message);
    }

    public UnauthorizedException(String code, String message) {
        super(HttpStatus.UNAUTHORIZED,  code, message);
    }
}

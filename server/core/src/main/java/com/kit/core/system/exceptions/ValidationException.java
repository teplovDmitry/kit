package com.kit.core.system.exceptions;

import org.springframework.http.HttpStatus;

public class ValidationException extends ApiException{
    public ValidationException(String message) {
        this("invalid", message);
    }

    public ValidationException(String code, String message) {
        super(HttpStatus.BAD_REQUEST, code, message);
    }
}

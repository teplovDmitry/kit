package com.kit.core.modules.user_role_permission.user.login;

import lombok.Data;

@Data
class LoginDto {
    private String email;
    private String password;
}

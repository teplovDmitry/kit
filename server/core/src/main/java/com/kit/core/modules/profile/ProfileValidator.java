package com.kit.core.modules.profile;

import com.kit.core.modules.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

@Validator
public class ProfileValidator {

    private final ProfileMapper profileMapper;

    @Autowired
    public ProfileValidator(ProfileMapper profileMapper) {
        this.profileMapper = profileMapper;
    }

    public Profile getValidEntity(ProfileDetailsDto dto) {
        Assert.isTrue(!StringUtils.isEmpty(dto.getFirstName()), "First name of profile is required");

        return this.profileMapper.mapToEntity(dto);
    }
}

package com.kit.core.modules.profile;

import com.kit.core.modules.Repository;

import java.util.Optional;

public interface ProfileRepository extends Repository<Profile> {

    Optional<Profile> findByLitresCode(String litresCode);
}

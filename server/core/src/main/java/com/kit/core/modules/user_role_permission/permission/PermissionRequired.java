package com.kit.core.modules.user_role_permission.permission;

import com.kit.core.modules.ModuleType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PermissionRequired {

    ModuleType moduleType();

    String name();

    String description();
}

package com.kit.core.modules.user_role_permission.user;

import com.kit.core.modules.Repository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends Repository<User> {
    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String email);
    List<User> findAll();
}

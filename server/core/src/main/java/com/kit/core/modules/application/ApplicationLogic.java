package com.kit.core.modules.application;

import com.kit.core.system.dto.BaseDto;
import com.kit.core.system.helpers.ContextHelper;
import com.kit.core.system.json.ExtraJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ApplicationLogic {

    private final ApplicationService applicationService;
    private final ApplicationHistoryService applicationHistoryService;

    @Autowired
    public ApplicationLogic(ApplicationService applicationService,
                            ApplicationHistoryService applicationHistoryService) {
        this.applicationService = applicationService;
        this.applicationHistoryService = applicationHistoryService;
    }

    @Transactional
    public Application createRequest(ApplicationType type, BaseDto dto) {
        ExtraJson content = ContextHelper.getApplicationHandler(type).getContent(dto);
        Application application = new Application(type, content);
        return this.applicationService.save(application);
    }
}

import { AfterViewInit, Component } from '@angular/core';
import '../../../../assets/login-animation.js';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../core.reducer';
import * as fromStore from '../../../store';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements AfterViewInit {
  email: string;
  password: string;

  constructor(private store$: Store<fromRoot.State>) {
  }

  ngAfterViewInit() {
    (window as any).initialize();
  }

  login(data) {
    console.log(data.value);
    this.store$.dispatch(new fromStore.Login(data.value));
  }
}

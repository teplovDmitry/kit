import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { MatSidenavModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { AppComponent } from './app.component';
import { LoginComponent } from './containers/entry/login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { coreEffects } from './core.effects';
import { HttpClientModule } from '@angular/common/http';
import { reducers } from './core.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { MylibHeaderComponent } from './core/mylib-header/mylib-header.component';
import { MylibNavigationComponent } from './core/mylib-navigation/mylib-navigation.component';
import { AuthorsListComponent } from './containers/authors/authors-list/authors-list.component';
import { BooksListComponent } from './containers/books/books-list/books-list.component';
import { CustomMaterialModule } from './core/material.module';
import { PageNotFoundComponent } from './core/not-found/page-not-found.component';
import { AboutComponent } from './containers/system/about/about.component';
// import { storeFreeze } from 'ngrx-store-freeze';
// import { environment } from '../environments/environment';

const appRoutes: Routes = [
  { path: '', redirectTo: '/authors', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'authors', component: AuthorsListComponent },
  { path: 'books', component: BooksListComponent },
  { path: 'about', component: AboutComponent },
  { path: '**', component: PageNotFoundComponent },
];

// todo that's more correctly, but why it's angry?
// export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : [];
export const metaReducers: MetaReducer<any>[] = [];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MylibHeaderComponent,
    MylibNavigationComponent,
    AuthorsListComponent,
    BooksListComponent,
    PageNotFoundComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    StoreModule.forRoot({}, {metaReducers}),
    EffectsModule.forRoot(coreEffects),
    HttpClientModule,
    StoreModule.forFeature('appStates', reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 10
    }),
    CustomMaterialModule,
    MatSidenavModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

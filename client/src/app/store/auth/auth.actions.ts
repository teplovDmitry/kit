import { Action } from '@ngrx/store';

export interface Credentials {
  username: string;
  password: string;
}

export const LOGIN = '[AUTH] LOGIN';
export const PURGE_AUTHENTICATE = '[AUTH] PURGE_AUTHENTICATE';
export const CHECK_AUTH = '[AUTH] CHECK_AUTH';
export const SAVE_TOKEN = '[AUTH] SAVE_TOKEN';

export class Login implements Action {
  readonly type = LOGIN;

  constructor(public payload?: Credentials) {
  }
}

export class CheckAuth implements Action {
  readonly type = CHECK_AUTH;

  constructor(public payload?: Credentials) {
  }
}

export class SaveToken implements Action {
  readonly type = SAVE_TOKEN;

  constructor(public payload?: any) {
  }
}

export class PurgeAuth implements Action {
  readonly type = PURGE_AUTHENTICATE;

  constructor(public payload?: Credentials) {
  }
}

export type AuthActions = Login | SaveToken | CheckAuth | PurgeAuth;

import { Injectable } from '@angular/core';
import { of as observableOf } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import * as authActions from './auth.actions';
import { Router } from '@angular/router';
import { AuthState } from './auth.reducer';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { AuthService } from './auth.service';
import { NgRxAction } from '../action.interface';

@Injectable()
export class AuthEffects {
  private _window: Window;

  @Effect()
  check_auth$ = this.actions$.pipe(
    ofType(authActions.CHECK_AUTH),
    map((action: NgRxAction) => {
      const token = this._window.localStorage.getItem('token');
      if (token) {
        return new authActions.SaveToken(token);
      }
      return new authActions.PurgeAuth();
    }));

  @Effect()
  login$ = this.actions$.pipe(
    ofType(authActions.LOGIN),
    switchMap((action: authActions.AuthActions) => {
      return this.authService.login(action.payload)
        .pipe(map((res: any) => {
            this._window.localStorage.setItem('token', action.payload);
            return new authActions.SaveToken(res.data);
          }),
          catchError(res => {
            this.store$.dispatch(new authActions.PurgeAuth());
            return observableOf({type: 'your_action'});
          }));
    }));

  @Effect()
  save_token$ = this.actions$.pipe(
    ofType(authActions.SAVE_TOKEN),
    map((action: NgRxAction) => {
      if (action.payload) {
        this._window.localStorage.setItem('token', action.payload);
        return {type: 'your_action'};
      }
      return new authActions.PurgeAuth();
    }));

  constructor(private store$: Store<AuthState>,
              private router: Router,
              private authService: AuthService,
              private actions$: Actions) {
  }
}

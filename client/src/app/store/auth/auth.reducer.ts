import * as fromAuth from './auth.actions';

export type AuthState = Map<string, any>;

const initialState: AuthState = new Map<string, any>();

export function authReducer(state = initialState, action: fromAuth.AuthActions) {
  switch (action.type) {
    case fromAuth.LOGIN:
      console.log('Auth reducer:', action);
      return new Map<string, any>();
    case fromAuth.SAVE_TOKEN:
      console.log(action);
      return new Map<string, any>();
    default:
      return state;
  }
}

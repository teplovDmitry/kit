import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { environment } from '../../../environments/environment';

@Injectable({providedIn: 'root'})
export class AuthService {
  constructor(private http: HttpClient) {
  }

  private getHeaders(): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*'
    };
    return new HttpHeaders(headersConfig);
  }

  public login(credentials: any): Observable<any> {
    return this.http.post(
      `${environment.API_ENDPOINT}login`,
      JSON.stringify(credentials),
      {headers: this.getHeaders()});
  }
}

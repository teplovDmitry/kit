import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as store from './store';

export interface State {
  auth: store.AuthState;
}

export const reducers: ActionReducerMap<State> = {
  auth: store.authReducer
};

export const getStates = createFeatureSelector<State>(
  'appStates'
);

export const login = createSelector(getStates, (state: State) => state.auth);

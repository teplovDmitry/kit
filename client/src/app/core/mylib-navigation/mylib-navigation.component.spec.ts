import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MylibNavigationComponent } from './mylib-navigation.component';

describe('MylibNavigationComponent', () => {
  let component: MylibNavigationComponent;
  let fixture: ComponentFixture<MylibNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MylibNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MylibNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MylibHeaderComponent } from './mylib-header.component';

describe('MylibHeaderComponent', () => {
  let component: MylibHeaderComponent;
  let fixture: ComponentFixture<MylibHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MylibHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MylibHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
